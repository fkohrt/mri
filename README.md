# Visualization of magnetic resonance imaging data

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fkohrt%2Fmri/master?filepath=Start.ipynb)

## License

Everything here available under MIT License, see [LICENSE](LICENSE). Builds on [NeuroSynth data](https://github.com/neurosynth/neurosynth-data), which are available under [ODbL v1.0](https://opendatacommons.org/licenses/odbl/1.0/).
